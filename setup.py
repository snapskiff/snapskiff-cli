from setuptools import setup

setup(
    name="snapskiff",
    version="0.0.1",
    install_requires=["requests"],
    packages=["snapskiff"],
    entry_points={"console_scripts": ["snapskiff = snapskiff.__main__:main"]},
)
