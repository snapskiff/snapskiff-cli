import argparse
import subprocess
import sys
from os import environ
from shutil import copyfileobj
from tempfile import NamedTemporaryFile
from urllib.request import urlopen

import requests


def main():
    parser = argparse.ArgumentParser(description="SnapSkiff Client")
    parser.add_argument(
        "--url", help="Custom server root URL", dest="override_root_url"
    )
    subparsers = parser.add_subparsers(required=True, dest="command")

    install_parser = subparsers.add_parser("install", help="Install a package by name")
    install_parser.add_argument("name", help="Name of package to install")

    args = parser.parse_args()

    print("Time to install " + args.name)

    if args.override_root_url:
        root_url = args.override_root_url
    else:
        root_url = "https://www.snapskiff.com"

    current_arch = environ["SNAP_ARCH"]

    if args.command == "install":
        packages_url = root_url + "/api/v1/packages.json"

        r = requests.get(packages_url)
        packages = r.json()

        desired_package = packages["packages"][args.name]
        channel = desired_package["default-channel"]
        version = desired_package["channels"][channel]
        file_info = desired_package["versions"][version][current_arch]

        file_download_url = root_url + file_info["download"]
        file_checksum = file_info["sha256"]

        with urlopen(file_download_url) as fsrc, NamedTemporaryFile(
            delete=True
        ) as fdst:
            print("Downloading")
            copyfileobj(fsrc, fdst)

            print("Verifying")
            sha256 = subprocess.run(
                ["sha256sum", "-z", fdst.name], capture_output=True
            ).stdout
            sha256 = sha256.split(b" ")[0]
            if file_checksum != sha256.decode():
                print("File checksums didn't match!")
                sys.exit(1)

            print("Installing")
            subprocess.run(["snap", "install", "--dangerous", fdst.name])
