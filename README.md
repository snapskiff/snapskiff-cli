# SnapSkiff Client

SnapSkiff is the simplest possible alternative to the centralized Snap Store. The centralized store is replaced with another, more different centralized store.

This repository contains the SnapSkiff client which can query a server for packages then download and install them.

This initial release of the SnapSkiff client can:

* Query https://www.snapskiff.com/ to find packages
* Install packages

There is currently only one package to install, 'snapskiff-helloworld'. When run, it produces the output "Hello world!"

```sh
$ sudo snapskiff install snapskiff-helloworld
Time to install snapskiff-helloworld
Downloading
Verifying
Verified
Installing
snapskiff-helloworld 0.0.1 installed
$ snapskiff-helloworld
Hello world!
```
